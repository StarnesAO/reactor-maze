#Dijkstra's
#set starting node
#set rad values to reach each node from start to infinity
#choose the node with the smallest value as current and visit all of its neighbours, updating their estimated distance from start
#once all neighbors of current node are visited and distances updated, set current node to visited
#iterate over entire maze

import turtle
import time
import sys
import math
from collections import deque
import generator


window = turtle.Screen()
window.bgcolor("black")
window.title("maze")
window.setup(1200, 1200)



class Maze(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square")
        self.color("white")
        self.penup()
        self.speed(0)

class Start(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("green")
        self.penup()
        self.speed(0)

class Finish(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("red")
        self.penup()
        self.speed(0)

class Search(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("blue")
        self.penup()
        self.speed(0)

class SolutionPath(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("gold")
        self.penup()
        self.speed(0)
        ## make size smaller to better show path ##

class Reactor(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("circle")
        self.color("red")
        self.penup()
        self.speed(0)

class Radiation(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("circle")
        self.turtlesize(.8,.8)
        self.color("red")
        self.penup()
        self.speed(0)


maze1 = [
    "#########################################",
    "#    ###############################    #",
    "# # #                                ## #",
    "# #S# ############################## ## #",
    "# ### #######        ###   ######### ## #",
    "#     #### ####### #  #  # ######### ## #",
    "## ####            ## # #            ## #",
    "## ## ####### ######R # ############### #",
    "##            ###   # # ########## #### #",
    "##### ########### # # #          #    # #",
    "##### ##   ##   # # # ########## #### # #",
    "##### ## # ## # # # # #####           # #",
    "#####    #    #   #   #####E########### #",
    "#########################################",
]
mazetest = [
    "#####",
    "#S#E#",
    "# # #",
    "#   R",
    "#####",
]
radtest = [ # low rad biased radtest
    "##############################",
    "#S                           #",
    "# ########################## #",
    "# ########################## #",
    "# ########################## #",
    "#  ######################### #",
    "##  ######################## #",
    "###  ####################### #",
    "####  ###################### #",
    "#####  ##################### #",
    "######  #################### #",
    "#######  ################### #",
    "########  ################## #",
    "######### ################## #",
    "######### ################## #",
    "######### ################## #",
    "########  ################## #",
    "#######  ################### #",
    "######  #################### #",
    "#####  ##################### #",
    "####  ###################### #",
    "###  ####################### #",
    "##  ######################## #",
    "#  ######################### #",
    "# ########################## #",
    "# ########################## #",
    "# ########################## #",
    "# ########################## #",
    "#E                           #",
    "##############################",
]
radtest2 = [ # low distance biased rad test
    "##############################",
    "#S                           #",
    "# ########################## #",
    "# ###                        #",
    "# ### ########################",
    "#  ##                        #",
    "##  ######################## #",
    "###  ####                    #",
    "####  ### ####################",
    "#####  ##                    #",
    "######  #################### #",
    "#######  ################### #",
    "########  ################## #",
    "######### ################## #",
    "######### ################## #",
    "######### ################## #",
    "########  ################## #",
    "#######  ################### #",
    "######  #################### #",
    "#####  ##                    #",
    "####  ### ####################",
    "###  ####                    #",
    "##  ######################## #",
    "#  ##                        #",
    "# ### ########################",
    "# ###                        #",
    "# ########################## #",
    "# ########################## #",
    "#E                           #",
    "##############################",
]
smallradtest = [
    "####",
    "#S #",
    "#ER#",
    "####"
]

random_maze, radiation_distance, reactor = generator.generate(30,30)

def find_reactor(radtest):
    global reactor
    for y in radtest:
        for x in y:
            if (x, y) == reactor:
                radtest[y][x].replace("#", "R")



radiation_level = 1


def setup_maze(grid):
    global start_x, start_y, end_x, end_y, reactor_x, reactor_y
    #rad_dict = dict()

    for y in range(len(grid)):

        for x in range(len(grid[y])):
            character = grid[y][x]
            screen_x =  -360 + (x * 24)
            screen_y =  360 - (y * 24)

            if character == "#":
                maze.goto(screen_x, screen_y)
                maze.stamp()
                walls.append((screen_x, screen_y))

            if character == " " or character == "E":
                path.append((screen_x, screen_y))

            if character == "E":
                finish.goto(screen_x, screen_y)
                finish.showturtle()
                end_x, end_y = screen_x, screen_y
                finish.stamp()

            if character == "S":
                start.goto(screen_x,screen_y)
                start.showturtle()
                start_x, start_y = screen_x, screen_y
                start.stamp()

            if character == "R":
                walls.append((screen_x, screen_y))
                reactor.goto(screen_x, screen_y)
                reactor_x, reactor_y = screen_x, screen_y
                reactor.showturtle()
                reactor.stamp()





def setup_radiation(grid):
    global start_x, start_y, end_x, end_y, reactor_x, reactor_y, rad_dict, total_rad
    rad_dict = dict()
    total_rad = dict()

    for y in range(len(grid)):

        for x in range(len(grid[y])):
            character = grid[y][x]
            screen_x =  -360 + (x * 24)
            screen_y =  360 - (y * 24)
            if character == "S":
                total_rad[(screen_x, screen_y)] = 0
            else:
                total_rad[(screen_x, screen_y)] = math.inf
            if character == " " or character == "E" or character == "S":
                rad_dict[(screen_x, screen_y)] = 1/radiation_distance[y][x]
                if (1/radiation_distance[y][x]) >= (1/18): # (1/18) approximately 3 diagonal tiles
                    radiation.goto(screen_x, screen_y)
                    radiation.color("tomato")
                    radiation.turtlesize(.25,.25)
                    radiation.stamp()
                elif (1/radiation_distance[y][x]) >= (1/64): # (1/63) approximately 6 diagonal tiles
                    radiation.goto(screen_x, screen_y)
                    radiation.color("coral")
                    radiation.turtlesize(.25,.25)
                    radiation.stamp()
                elif (1/radiation_distance[y][x]) >= (1/162): # (1/161) approximately 9 diagonal tiles
                    radiation.goto(screen_x, screen_y)
                    radiation.color("peach puff")
                    radiation.turtlesize(.15,.15)
                    radiation.stamp()



def endProgram():
    window.exitonclick()
    sys.exit()


def solve(x,y):
    global rad_dict, total_rad
    print("********")
    print("solve x,y: ", x, y)
    peripheral.append((x,y))
    solution[x,y] = x,y
    peripheral_radiation[(x,y)] = 0 ## double check y/x becaues it works but is weird
    print("********")
    print("peripheral_radiation (x,y)", peripheral_radiation[(x,y)])
    print("********")
    print("rad_dict: ", rad_dict[(x,y)])
    print("**********")
    print("total_rad before solving: ", total_rad)
    while len(peripheral_radiation) > 0:
    #while(x,y) != (end_x, end_y):
        x,y = min(peripheral_radiation, key=peripheral_radiation.get)
        del peripheral_radiation[(x,y)]

        if(x-24, y) in path and (x-24, y) not in visited:   ## CHECK LEFT ##
            cell = (x-24, y)
            solution[cell] = x,y
            peripheral_radiation[cell] = rad_dict[(x-24,y)] ## double check x/y




            ### This is the essence of Dijkstra's Algorithm
            if total_rad[cell] == math.inf:
                ### if we HAVE NOT seen the node, meaning the total radiation is set to infinity,
                ### set the total radiation to the total radiation from the previous node + the radiation value for the current node
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            elif total_rad[cell] > total_rad[(x,y)] + rad_dict[cell]:
                ### if we HAVE seen the node, but it's radiation through another path is longer, update the node with the lesser radiation
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]





            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x-24, y))

        if(x, y-24) in path and (x, y-24) not in visited:   ## CHECK DOWN ##
            cell = (x, y-24)
            solution[cell] = x,y
            peripheral_radiation[cell] = rad_dict[(x, y-24)]
            ### This is the essence of Dijkstra's Algorithm
            if total_rad[cell] == math.inf:
                ### if we HAVE NOT seen the node, meaning the total radiation is set to infinity,
                ### set the total radiation to the total radiation from the previous node + the radiation value for the current node
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            elif total_rad[cell] > total_rad[(x,y)] + rad_dict[cell]:
                ### if we HAVE seen the node, but it's radiation through another path is longer, update the node with the lesser radiation
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x, y-24))

        if(x+24, y) in path and (x+24, y) not in visited:   ## CHECK RIGHT ##
            cell = (x+24, y)
            solution[cell] = x,y
            peripheral_radiation[cell] = rad_dict[(x+24,y)]
            ### This is the essence of Dijkstra's Algorithm
            if total_rad[cell] == math.inf:
                ### if we HAVE NOT seen the node, meaning the total radiation is set to infinity,
                ### set the total radiation to the total radiation from the previous node + the radiation value for the current node
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            elif total_rad[cell] > total_rad[(x,y)] + rad_dict[cell]:
                ### if we HAVE seen the node, but it's radiation through another path is longer, update the node with the lesser radiation
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x+24, y))

        if(x, y+24) in path and (x, y+24) not in visited:   ## CHECK UP ##
            cell = (x, y+24)
            solution[cell] = x,y
            peripheral_radiation[cell] = rad_dict[(x, y+24)]
            ### This is the essence of Dijkstra's Algorithm
            if total_rad[cell] == math.inf:
                ### if we HAVE NOT seen the node, meaning the total radiation is set to infinity,
                ### set the total radiation to the total radiation from the previous node + the radiation value for the current node
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            elif total_rad[cell] > total_rad[(x,y)] + rad_dict[cell]:
                ### if we HAVE seen the node, but it's radiation through another path is longer, update the node with the lesser radiation
                total_rad[cell] = total_rad[(x,y)] + rad_dict[cell]
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x, y+24))

def backRoute(x,y):
    solutionpath.goto(x,y)
    solutionpath.showturtle()
    solutionpath.stamp()
    while(x,y) != (start_x, start_y):
        solutionpath.goto(solution[x,y])
        solutionpath.stamp()
        x,y = solution[x,y]

maze = Maze()
start = Start()
finish = Finish()
search = Search()
solutionpath = SolutionPath()
reactor = Reactor()
radiation = Radiation()


walls = []
path = []
peripheral = deque()
visited = set()
solution = {}



peripheral_radiation = dict()
total_rad = dict()

#maze_to_solve = radtest #lowest rad biased

#maze_to_solve = radtest2  #shortest path biased

maze_to_solve = random_maze

setup_maze(maze_to_solve)
find_reactor(maze_to_solve)
setup_radiation(maze_to_solve)
solve(start_x, start_y)
print("*************")
print("\n")
print("\n")
print("Total_rad after solving: ", total_rad)
backRoute(end_x, end_y)
window.exitonclick()
