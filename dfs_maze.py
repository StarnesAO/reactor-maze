import turtle
import time
import sys
from collections import deque
import generator


window = turtle.Screen()
window.bgcolor("black")
window.title("maze")
window.setup(1200,1200)


class Maze(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square")
        self.color("white")
        self.penup()
        self.speed(0)

class Start(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("green")
        self.penup()
        self.speed(0)

class Finish(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("red")
        self.penup()
        self.speed(0)

class Search(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("blue")
        self.penup()
        self.speed(0)

class SolutionPath(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("gold")
        self.penup()
        self.speed(0)
        ## make size smaller to better show path ##

class Reactor(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("white")
        self.penup()
        self.speed(0)

maze1 = [
    "#########################################",
    "#    ###############################    #",
    "# # #                                ## #",
    "# #S# ############################## ## #",
    "# ### #######        ###   ######### ## #",
    "#     #### ####### #  #  # ######### ## #",
    "## ####            ## # #            ## #",
    "## ## ####### ####### # ############### #",
    "##            ###   # # ########## #### #",
    "##### ########### # # #          #    # #",
    "##### ##   ##   # # # ########## #### # #",
    "##### ## # ## # # # # #####           # #",
    "#####    #    #   #   #####E########### #",
    "#########################################",
]
mazetest = [
    "#####",
    "#S#E#",
    "# # #",
    "#   R",
    "#####",
]

radtest = [
    "###########",
    "#S        #",
    "#         #",
    "#    R    #",
    "#         #",
    "#        E#",
    "###########",
]

random_maze, radiation_distance, reactor = generator.generate(30,30)

def setup_maze(grid):
    global start_x, start_y, end_x, end_y, reactor_x, reactor_y
    for y in range(len(grid)):
        for x in range(len(grid[y])):
            character = grid[y][x]
            screen_x =  -360 + (x * 24)
            screen_y =  360 - (y * 24)
            if character == "#":
                maze.goto(screen_x, screen_y)
                maze.stamp()
                walls.append((screen_x, screen_y))

            if character == " " or character == "E":
                path.append((screen_x, screen_y))

            if character == "E":
                finish.goto(screen_x, screen_y)
                finish.showturtle()
                end_x, end_y = screen_x, screen_y
                finish.stamp()

            if character == "S":
                start.goto(screen_x,screen_y)
                start.showturtle()
                start_x, start_y = screen_x, screen_y
                start.stamp()

            if character == "R":
                walls.append((screen_x, screen_y))
                reactor.goto(screen_x, screen_y)
                reactor_x, reactor_y = screen_x, screen_y
                reactor.showturtle()
                reactor.stamp()

def endProgram():
    window.exitonclick()
    sys.exit()

def dfs_search(x, y):
    #turtle.delay(1000) Delay for explanations
    stack.append((x,y))
    solution[x,y] = x,y
    #while len(stack) > 0:
    while(x,y) != (end_x, end_y):
        x,y = stack.pop()
        search.goto((x,y))
        search.showturtle()
        search.stamp()
        if (x, y - 24) in path:
            if (x, y - 24) not in visited:
                cell = (x, y -24)
                solution[cell] = x,y
                stack.append(cell)

                visited.add((x,y-24))
        if (x-24, y) in path:
            if (x-24, y) not in visited:
                cell = (x-24, y)
                solution[cell] = x,y
                stack.append(cell)

                visited.add((x-24,y))
        if (x+24, y) in path:
            if (x+24, y) not in visited:
                cell = (x+24, y)
                solution[cell] = x,y
                stack.append(cell)

                visited.add((x+24,y))
        if (x, 24 + y) in path:
            if (x, 24 + y) not in visited:
                cell = (x, 24 + y)
                solution[cell] = x,y
                stack.append(cell)

                visited.add((x, 24 +y))

def backRoute(x,y):
    solutionpath.goto(x,y)
    solutionpath.showturtle()
    solutionpath.stamp()
    while(x,y) != (start_x, start_y):
        solutionpath.goto(solution[x,y])
        solutionpath.stamp()
        x,y = solution[x,y]






maze = Maze()
start = Start()
finish = Finish()
search = Search()
solutionpath = SolutionPath()
reactor = Reactor()

solution = {}
visited = set()
stack = deque()
walls = []
path = []




setup_maze(random_maze)
dfs_search(start_x, start_y)
backRoute(end_x, end_y)
window.exitonclick()
