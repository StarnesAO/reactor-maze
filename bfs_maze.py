import turtle
import time
import sys
from math import sqrt
from collections import deque
import generator

###GRAPHIC WINDOW####
window = turtle.Screen()
window.bgcolor("black")
window.title("maze")
window.setup(1200, 1200)



class Maze(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square")
        self.color("white")
        self.penup()
        self.speed(0)

class Start(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("green")
        self.penup()
        self.speed(0)

class Finish(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("red")
        self.penup()
        self.speed(0)

class Search(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("blue")
        self.penup()
        self.speed(0)

class SolutionPath(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("gold")
        self.penup()
        self.speed(0)
        ## make size smaller to better show path ##

class Reactor(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("orange")
        self.penup()
        self.speed(0)

class Radiation(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.hideturtle()
        self.shape("square")
        self.color("teal")
        self.penup()
        self.speed(0)


maze1 = [
    "#########################################",
    "#    ###############################    #",
    "# # #                                ## #",
    "# #S# ############################## ## #",
    "# ### #######        ###   ######### ## #",
    "#     #### ####### #  #  # ######### ## #",
    "## ####            ## # #            ## #",
    "## ## ####### ######R # ############### #",
    "##            ###   # # ########## #### #",
    "##### ########### # # #          #    # #",
    "##### ##   ##   # # # ########## #### # #",
    "##### ## # ## # # # # #####           # #",
    "#####    #    #   #   #####E########### #",
    "#########################################",
]
mazetest = [
    "#####",
    "#S#E#",
    "# # #",
    "#   R",
    "#####",
]
radtest = [
    "###########",
    "#S        #",
    "#         #",
    "#         #",
    "#         #",
    "#    R    #",
    "#         #",
    "#         #",
    "#         #",
    "#        E#",
    "###########",
]
smallradtest = [
    "####",
    "#S #",
    "#ER#",
    "####"
]

random_maze, radiation_distance, reactor = generator.generate(30,30)
radiation_level = 1

def setup_maze(grid):
    global start_x, start_y, end_x, end_y, reactor_x, reactor_y
    #rad_dict = dict()

    for y in range(len(grid)):

        for x in range(len(grid[y])):
            character = grid[y][x]
            screen_x =  -360 + (x * 24)
            screen_y =  360 - (y * 24)

            if character == "#":
                maze.goto(screen_x, screen_y)
                maze.stamp()
                walls.append((screen_x, screen_y))

            if character == " " or character == "E":
                path.append((screen_x, screen_y))

            if character == "E":
                finish.goto(screen_x, screen_y)
                finish.showturtle()
                end_x, end_y = screen_x, screen_y
                finish.stamp()

            if character == "S":
                start.goto(screen_x,screen_y)
                start.showturtle()
                start_x, start_y = screen_x, screen_y
                start.stamp()

            if character == "R":
                walls.append((screen_x, screen_y))
                maze.goto(screen_x, screen_y)
                maze.stamp()
                """reactor.goto(screen_x, screen_y)
                reactor_x, reactor_y = screen_x, screen_y
                reactor.showturtle()
                reactor.stamp()"""

def setup_radiation(random_maze):
    global start_x, start_y, end_x, end_y, reactor_x, reactor_y
    #rad_dict = dict()

    for y in range(len(random_maze)):

        for x in range(len(random_maze[y])):
            character = random_maze[y][x]
            screen_x =  -360 + (x * 24)
            screen_y =  360 - (y * 24)

            if character == " " or character == "E":
                if (1/radiation_distance[y][x]) >= (1/161): # approximately 9 diagonal tiles
                    radiation.goto(screen_x, screen_y)
                    radiation.stamp()



def endProgram():
    window.exitonclick()
    sys.exit()


def solve(x,y):

    #turtle.delay(1000) # slows down the blue search square
    peripheral.append((x,y))
    solution[x,y] = x,y

    while len(peripheral) > 0:
    #while(x,y) != (end_x, end_y):
        x,y = peripheral.popleft()

        if(x-24, y) in path and (x-24, y) not in visited:   ## CHECK LEFT ##
            cell = (x-24, y)
            solution[cell] = x,y
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x-24, y))

        if(x, y-24) in path and (x, y-24) not in visited:   ## CHECK DOWN ##
            cell = (x, y-24)
            solution[cell] = x,y
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x, y-24))

        if(x+24, y) in path and (x+24, y) not in visited:   ## CHECK RIGHT ##
            cell = (x+24, y)
            solution[cell] = x,y
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x+24, y))

        if(x, y+24) in path and (x, y+24) not in visited:   ## CHECK UP ##
            cell = (x, y+24)
            solution[cell] = x,y
            search.goto(cell)
            search.showturtle()
            search.stamp()
            peripheral.append(cell)
            visited.add((x, y+24))

def backRoute(x,y):
    solutionpath.goto(x,y)
    solutionpath.showturtle()
    solutionpath.stamp()
    while(x,y) != (start_x, start_y):
        solutionpath.goto(solution[x,y])
        solutionpath.stamp()
        x,y = solution[x,y]

maze = Maze()
start = Start()
finish = Finish()
search = Search()
solutionpath = SolutionPath()
reactor = Reactor()
radiation = Radiation()


walls = []
path = []
peripheral = deque()
visited = set()
solution = {}


peripheral_radiation = deque()
visited_radiation = set()



setup_maze(random_maze)
#setup_radiation(random_maze)
#button = Button(tkWindow, text = "Solve", command = lambda: solve(start_x, start_y))

solve(start_x, start_y)
backRoute(end_x, end_y)
endProgram()
