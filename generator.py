import random

#import time


def surroundingCells(random_wall):
    s_cells = 0
    if (maze[random_wall[0]-1][random_wall[1]] == " "):
        #if left tile is path, surrounding cells increases
        s_cells += 1
    if (maze[random_wall[0]+1][random_wall[1]] == " "):
        #if right tile is path, surrounding cells increases
        s_cells += 1
    if (maze[random_wall[0]][random_wall[1]-1] == " "):
        #lower tile
        s_cells += 1
    if (maze[random_wall[0]][random_wall[1]+1] == " "):
        #upper tile
        s_cells += 1
    return s_cells

def make_walls(width, height):                   ### turns unexplored tiles "*" into walls "#"
    for i in range(0, height):
        for j in range(0, width):
            if maze[i][j] == "*":
                maze[i][j] = "#"

def create_start_exit(width, height):
    for i in range(0, width):
        if (maze[1][i] == " "):
            maze[0][i] = "S"                   ### (may need to play with this) ###
            break
    for i in range(width-1,0,-1):
        if maze[height-2][i] == " ":
            maze[height-1][i] = "E"           ### (may need to play with this) ###
            break
def create_reactor(width, height):
    for i in range(0, width):
        if maze[height//2][width//2+i] == "#":
            maze[height//2][width//2+i] = "R"
            break

def string_convert(maze):
    for line in maze:
        empty_line = ""
        for char in line:
            empty_line += char
        converted_maze.append(empty_line)
    return converted_maze

def radiation_calc(maze):
    global radiation_distance, row
    x = -1
    y = -1
    sub_distance = []
    reference = []
    radiation_distance = []


    for row in maze:
        global reactor
        y += 1
        for char in row:
            x += 1
            reference.append((x,y))
            if char == "R":
                reactor = (x,y)
            if x == len(row)-1:
                x = -1


    for position in reference:
        i = 1
        x_diff = abs(reactor[0] - position[0]) # a
        y_diff = abs(reactor[1] - position[1]) # b
        pythag = (x_diff**2)+(y_diff**2) # a^2 + b^2
        if pythag != 0:
            i += 1
            sub_distance.append(pythag)  # makes the distance a list of lists aka a matrix
           # rad_dict[reference] = ([1/pythag])
        else:
            i += 1
            sub_distance.append(pythag)
        if position[0] == len(row)-1:
            radiation_distance.append(sub_distance)
            sub_distance = []

def generate(width, height):
    create_maze(width, height)  # makes list of strings * equal to wxh
    make_walls(width, height)  # turns * into #
    create_start_exit(width, height) # adds S and E to list
    create_reactor(width, height) # adds R to list
    string_convert(maze) # converts multiple lists to 1 list
    radiation_calc(converted_maze)
    return converted_maze, radiation_distance, reactor


converted_maze = []
cell = " "
wall = "#"
unvisited = "*"
height = 4
width = 4
maze = []
rad_dict = dict()

def create_maze(width, height):

    for x in range(0, height):
        line = []
        for y in range(0, width):
            line.append("*")
        maze.append(line)



    starting_height = int(random.random()*height)
    starting_width = int(random.random()*width)
    if (starting_height == 0):
        starting_height += 1
    if (starting_height == height-1):
        starting_height -= 1
    if (starting_width == 0):
        starting_width += 1
    if (starting_width == width-1):
        starting_width -= 1


    maze[starting_height][starting_width] = cell
    walls = []
    walls.append([starting_height - 1, starting_width])
    walls.append([starting_height, starting_width -1])
    walls.append([starting_height, starting_width + 1])
    walls.append([starting_height + 1, starting_width])

    maze[starting_height-1][starting_width] = "#"
    maze[starting_height][starting_width-1] = "#"
    maze[starting_height][starting_width+1] = "#"
    maze[starting_height+1][starting_width] = "#"

    ############# MAIN #################

    while walls:

        random_wall = walls[int(random.random()*len(walls))-1]  ## (mental note to change this to a shuffle for efficiency) ##


        if random_wall[1] != 0:
            if maze[random_wall[0]][random_wall[1]-1] == "*" and maze[random_wall[0]][random_wall[1]+1] == " ":      # unexplored tiles denoted by "*", path tiles denoted by " ", wall tiles denoted by "#"
                #if lower tile is unexplored and upper tile is path
                s_cells = surroundingCells(random_wall)                                                               #checks number of walls near cell
                if s_cells < 2:
                    #if there are less than 2 surrounding path tiles, designate tile as a path tile                                                                                       #ensure we only "dig" into walls and don't accidentally join tunnels
                    maze[random_wall[0]][random_wall[1]] = " "

                    if (random_wall[0] != 0): # upper
                        if (maze[random_wall[0]-1][random_wall[1]] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]-1][random_wall[1]] = "#"
                        if ([random_wall[0]-1, random_wall[1]] not in walls):
                            walls.append([random_wall[0]-1, random_wall[1]])

                    if (random_wall[0] != height -1): # bottom

                        if (maze[random_wall[0]+1][random_wall[1]] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]+1][random_wall[1]] = "#"
                        if ([random_wall[0]+1, random_wall[1]] not in walls):
                            walls.append([random_wall[0]+1, random_wall[1]])

                    if (random_wall[1] != 0): # left
                        if (maze[random_wall[0]][random_wall[1]-1] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]][random_wall[1]-1] = "#"
                        if ([random_wall[0], random_wall[1]-1] not in walls):
                            walls.append([random_wall[0], random_wall[1]-1])

                for wall in walls:
                    if (wall[0] == random_wall[0] and wall[1] == random_wall[1]):
                        walls.remove(wall)

                continue

        if random_wall[0] != 0:
            if maze[random_wall[0]-1][random_wall[1]] == "*" and maze[random_wall[0]+1][random_wall[1]] == " ":
                #if left tile is unexplored and right tile is path

                s_cells = surroundingCells(random_wall)
                if s_cells < 2:
                    #if there are less than 2 surrounding path tiles, designate tile as a path tile
                    maze[random_wall[0]][random_wall[1]] = " "

                    if (random_wall[0] != 0): #upper
                        if (maze[random_wall[0]-1][random_wall[1]] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]-1][random_wall[1]] = "#"
                        if ([random_wall[0]-1, random_wall[1]] not in walls):
                            walls.append([random_wall[0]-1, random_wall[1]])

                    if (random_wall[1] != width -1): #right
                        if (maze[random_wall[0]][random_wall[1]+1] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]][random_wall[1]+1] = "#"
                        if ([random_wall[0], random_wall[1]+1] not in walls):
                            walls.append([random_wall[0], random_wall[1]+1])

                    if (random_wall[1] != 0): #left
                        if (maze[random_wall[0]][random_wall[1]-1] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]][random_wall[1]-1] = "#"
                        if ([random_wall[0], random_wall[1]-1] not in walls):
                            walls.append([random_wall[0], random_wall[1]-1])

                for wall in walls:
                    if (wall[0] == random_wall[0] and wall[1] == random_wall[1]):
                        walls.remove(wall)

                continue

        if random_wall[0] != height - 1:
            if maze[random_wall[0]+1][random_wall[1]] == "*" and maze[random_wall[0]-1][random_wall[1]] == " ":
                #if right tile is unexplored and left tile is path
                s_cells = surroundingCells(random_wall)

                if s_cells < 2:
                    #if there are less than 2 surrounding path tiles, designate tile as a path tile
                    maze[random_wall[0]][random_wall[1]] = " "

                    if (random_wall[0] != height -1): #bottom

                        if (maze[random_wall[0]+1][random_wall[1]] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]+1][random_wall[1]] = "#"
                        if ([random_wall[0]+1, random_wall[1]] not in walls):
                            walls.append([random_wall[0]+1, random_wall[1]])

                    if (random_wall[1] != width -1): #right
                        if (maze[random_wall[0]][random_wall[1]+1] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]][random_wall[1]+1] = "#"
                        if ([random_wall[0], random_wall[1]+1] not in walls):
                            walls.append([random_wall[0], random_wall[1]+1])

                    if (random_wall[1] != 0): #left
                        if (maze[random_wall[0]][random_wall[1]-1] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]][random_wall[1]-1] = "#"
                        if ([random_wall[0], random_wall[1]-1] not in walls):
                            walls.append([random_wall[0], random_wall[1]-1])

                for wall in walls:
                    if (wall[0] == random_wall[0] and wall[1] == random_wall[1]):
                        walls.remove(wall)

                continue

        if random_wall[1] != width - 1:
            if maze[random_wall[0]][random_wall[1]+1] == "*" and maze[random_wall[0]][random_wall[1]-1] == " ":
                #if upper tile is unexplored and lower tile is path
                s_cells = surroundingCells(random_wall)
                if s_cells < 2:
                    #if there are less than 2 surrounding path tiles, designate tile as a path tile
                    maze[random_wall[0]][random_wall[1]] = " "

                    if (random_wall[0] != 0): #upper
                        if (maze[random_wall[0]-1][random_wall[1]] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]-1][random_wall[1]] = "#"
                        if ([random_wall[0]-1, random_wall[1]] not in walls):
                            walls.append([random_wall[0]-1, random_wall[1]])

                    if (random_wall[1] != width -1): #right
                        if (maze[random_wall[0]][random_wall[1]+1] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]][random_wall[1]+1] = "#"
                        if ([random_wall[0], random_wall[1]+1] not in walls):
                            walls.append([random_wall[0], random_wall[1]+1])

                    if (random_wall[0] != height -1): #bottom

                        if (maze[random_wall[0]+1][random_wall[1]] != " "):                                           # adds walls to list of walls
                            maze[random_wall[0]+1][random_wall[1]] = "#"
                        if ([random_wall[0]+1, random_wall[1]] not in walls):
                            walls.append([random_wall[0]+1, random_wall[1]])

                for wall in walls:
                    if (wall[0] == random_wall[0] and wall[1] == random_wall[1]):
                        walls.remove(wall)

                continue
        for wall in walls:
            if wall[0] == random_wall[0] and wall[1] == random_wall[1]:
                walls.remove(wall)
    return maze
